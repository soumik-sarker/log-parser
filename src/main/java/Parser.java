/**
 * @author soumik
 * @since ১৯/৭/২১
 */
import java.util.HashSet;
import java.util.Set;

public class Parser implements Comparable<Parser>{
    private String time;
    private int getCount, postCount, totalResponseTime, totalGetPostCount;
    private Set<String>urlList = new HashSet<String>();

    Parser(String time){
        this.time = time;
        this.getCount = 0;
        this.postCount = 0;
        this.totalResponseTime = 0;

        urlList.clear();

    }

    public void setGetCount(){
        getCount += 1;
        totalGetPostCount += 1;
    }

    public void setPostCount(){
        postCount += 1;
        totalGetPostCount += 1;
    }

    public void addUrl(String url){
        urlList.add(url);
    }

    public void setTotalResponseTime(int t){
        totalResponseTime += t;
    }


    public int compareTo(Parser p)
    {
        return this.totalGetPostCount - p.totalGetPostCount;
    }

    public void displayLogDetails(){

        System.out.format("%-25s%-20s%-20d%-10s\n" , time, getCount+"/"+postCount, urlList.size(), totalResponseTime+"ms");
    }

}