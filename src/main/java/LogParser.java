/**
 * @author soumik
 * @since ১৯/৭/২১
 */
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.BufferedReader;
import java.util.Collections;

public class LogParser {
    public static ArrayList<Parser>logList = new ArrayList<Parser>();
    public static void main(String[] args){

        try{
            FileReader f = new FileReader(args[0]);
            BufferedReader br = new BufferedReader(f);
            String line = "";
            String currentHourRange = "";
            Parser parser = null;

            while((line = br.readLine()) != null){

                //String line = br.readLine();

                if(!isProfiler(line).isEmpty()){

                    String time = getTime(line);
                    String hourRange = getHour(line);

                    int responseTime = getResponseTime(line);
                    String url = getUrl(line);

                    if (!currentHourRange.equals(hourRange)){
                        parser = new Parser(hourRange);
                        currentHourRange = hourRange;
                        logList.add(parser);

                        if (getResponseType(line).equals("G")){
                            parser.setGetCount();
                        }
                        else {
                            parser.setPostCount();
                        }
                        parser.addUrl(url);
                        parser.setTotalResponseTime(responseTime);
                    }

                    else{
                        if (getResponseType(line).equals("G")){
                            parser.setGetCount();
                        }
                        else {
                            parser.setPostCount();
                        }
                        parser.addUrl(url);
                        parser.setTotalResponseTime(responseTime);
                    }
                }

            }

            if (args.length < 2){
                displayLogSummary(false);
            }
            else {
                displayLogSummary(true);
            }

        } catch(IOException e){}
    }

    private static String getTime(String s) {
        String time = null;
        try {
            Date date = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(s);
            time = new SimpleDateFormat("hh:mm aa").format(date);

        } catch(ParseException e){
            System.out.println("Parse Exception Occured");
        }
        return time;
    }

    private static String getHour(String s) {
        String hour = null;

        try {
            Date date = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(s);
            hour = new SimpleDateFormat("hh:00 aa").format(date);

            Date addHour = new Date(date.getTime() + 1*60*60*1000);
            hour += " - " + new SimpleDateFormat("hh:00 aa").format(addHour);

        } catch(ParseException e){
            System.out.println("Parse Exception Occured");
        }
        return hour;
    }

    private static String getResponseType(String s){
        Pattern pattern = Pattern.compile("\\s+([\\w]+),");
        Matcher matcher = pattern.matcher(s);

        if(matcher.find()){
            //System.out.println(matcher.group(1));
            return matcher.group(1);
        }

        else return "";
    }

    private static int getResponseTime(String s){
        Pattern pattern = Pattern.compile("(\\d+)ms$");
        Matcher matcher = pattern.matcher(s);

        if(matcher.find()){
            //System.out.println(matcher.group(1));
            int time = Integer.parseInt(matcher.group(1));
            return time;
        }

        else return 0;
    }

    private static String getUrl(String s){
        Pattern pattern = Pattern.compile("URI[=]\\[+(\\/.*?(?=\\]))");
        Matcher matcher = pattern.matcher(s);

        if(matcher.find()){
            //System.out.println(matcher.group(1));
            return matcher.group(1);
        }

        else return null;
    }

    private static String isProfiler(String s){
        Pattern pattern = Pattern.compile("\\[PROFILER:\\d+\\]");
        Matcher matcher = pattern.matcher(s);

        if(matcher.find()){
            //System.out.println(matcher.group());
            return matcher.group();
        }

        else return "";
    }

    private static void displayLogSummary(boolean needSorted){
        System.out.println("------------------------------------------------------------------------------------\n");
        System.out.printf("%-25s%-20s%-20s%-10s\n" , "Time", "GET/POST" , "Unique", "Total");
        System.out.printf("%-25s%-20s%-20s%-10s" , "", "Count", "URI Count" , "Response Time");
        System.out.println();

        if (needSorted == true){
            Collections.sort(logList);
        }

        for(Parser p: logList){
            System.out.println("------------------------------------------------------------------------------------\n");
            p.displayLogDetails();
        }
        System.out.println("------------------------------------------------------------------------------------\n");

    }
}
